package lumivalgekejaseistsep6ialpoissi;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Kehamassiindeks extends Application {

	/*
	 * Kehamassiindeksi arvutamiseks jagatakse kehakaal kilogrammides pikkuse
	 * 
	 * ruuduga meetrites.
	 */

	private TextField p;
	private TextField k;
	private TextField c;

	public void start(Stage primaryStage) {

		// ala, kuhu asju paigutada.

		FlowPane pane = new FlowPane();
		pane.setPadding(new Insets(11, 12, 13, 14));
		pane.setAlignment(Pos.CENTER);

		Scene scene = new Scene(pane, 550, 500);

		// ma ei taha, et mul oleks textfieldid koos, seepärast kasutan
		// separator, mis eraldab.

		Separator separator = new Separator();

		p = new TextField();
		p.setPrefColumnCount(3);
		p.setStyle("-fx-background-color: white; -fx-border-color: yellow");

		// Pikkuse teksti sisestusala
		pane.getChildren().addAll(new Label("Pikkus(m): "), p);

		pane.getChildren().add(separator);

		k = new TextField();
		k.setPrefColumnCount(3);
		k.setStyle("-fx-background-color: white; -fx-border-color: yellow");

		pane.getChildren().addAll(new Label("Kaal(kg): "), k);

		// loon nupu

		Button ARVUTA = new Button("ARVUTA");
		ARVUTA.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				// läheb kehamass meetodisse kui arvuta nuppu vajutatakse

				kehamass();
			}
		});

		pane.setStyle("-fx-color: blue");
		pane.getChildren().addAll(ARVUTA);

		// ühte pilti on ka vaja sellele aknale!

		Image image = new Image(
				"http://kaaluabi.naistekas.ee/system/ckeditor_assets/pictures/549/content_belly-2354_640.jpg");
		ImageView imageView = new ImageView(image);
		imageView.setFitHeight(100);
		imageView.setFitWidth(100);
		pane.getChildren().addAll(new ImageView(image));

		// tekst

		Text text2 = new Text(160, 200, "Teie kehamassiindeks on: ");
		text2.setFill(Color.PINK);
		text2.setFont(Font.font("Times New Roman", FontWeight.BOLD, 25));

		pane.getChildren().add(text2);

		new TextField();
		c = new TextField();
		c.setPrefColumnCount(5);
		c.setAlignment(Pos.BOTTOM_RIGHT);
		pane.getChildren().add(c);
		c.setEditable(false);

		// lava "Kehamassiindeks"

		primaryStage.setTitle("Kehamassiindeks");
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	// kasutaja sisestused double-tüüpi numbriks ja arvutab indeksi

	public void kehamass() {

		double i = 0;
		double kaal = 0;

		// try-catch, selleks , et kui kasutaja sisestab midagi muud peale
		// numbri.

		try {
			kaal = Double.parseDouble(this.k.getText());
		} catch (NumberFormatException e) {
			e.getStackTrace();
		}

		double pikkus = 0;

		try {
			pikkus = Double.parseDouble(this.p.getText());
		} catch (NumberFormatException e) {
			e.getStackTrace();
		}

		try {
			i = kaal / (pikkus * pikkus);
			Math.round((i));

			// et komakohta ka näitaks

			double j = Math.round((i * 10)) / 10.0;
			c.setText(Double.toString(j));

		} catch (Exception e) {
			c.setText("tõrge");
		}

		/*
		 * kui indeks väiksem kui 19, siis väljastab "Te olete alakaalus!", kui
		 * suurem kui 25, siis "Te olete ülekaalus!" ja need tingimused pole
		 * täidetud, siis "Te olete normaalkaalus!"
		 */

		if (i < 19) {
			System.out.println("Te olete alakaalus");
		} else if (i > 25) {
			System.out.println("Te olete ülekaalus!");
		} else {
			System.out.println("Te olete normaalkaalus!");
			
		}
	}
}
