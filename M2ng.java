package lumivalgekejaseistsep6ialpoissi;

import javax.swing.*;

import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

public class M2ng extends JPanel implements ActionListener, KeyListener {

	private static final long serialVersionUID = 1L;

	// deklareerin, mida kasutan

	Timer t = new Timer(5, this);
	double x = 0, y = 0, velx = 0, vely = 0;
	public Color b = new Color(200, 20, 120);

	public boolean võidetud = false;

	ArrayList<Integer[]> pallid;

	// Konstruktor

	public M2ng() {
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		looPallid();
	}

	// meetod, mis loob pallid. pall on massiiv, mida on 7 tükki

	public void looPallid() {

		pallid = new ArrayList<Integer[]>();

		// tsükkel, mille tingimus on: kui i on väiksem kui 7, siis suurenda
		// seda 1 võrra.

		// Sees on massiiv nimega pall, milles on kaks komponenti

		for (int i = 0; i < 7; i++) {
			Integer[] pall = new Integer[2];
			pall[0] = (int) (Math.random() * 800);
			pall[1] = (int) (Math.random() * 600);
			pallid.add(pall);
		}
	}

	// meetod, millega tehakse graafikat.

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;

		// must ruut

		g2.fillRect(350, 200, 80, 80);
		t.start();

		// hallid pallid

		for (int i = 0; i < pallid.size(); i++) {
			Integer[] pall = pallid.get(i);
			if (i < 3) {
				g2.setColor(Color.GREEN);
			} else {
				g2.setColor(Color.GRAY);
			}
			g2.fillOval(pall[0], pall[1], 80, 80);
		}

		// sinine mängija pall

		g2.setColor(Color.blue);
		g2.fill(new Ellipse2D.Double(x, y, 40, 40));
		this.setBackground(b);

	}

	// kui vajutatakse klaviatuuril nuppu, siis...

	public void actionPerformed(ActionEvent e) {
		repaint();

		// kui tingimus võidetud ei ole täidetud, siis liida x-le ja y-le velx
		// ja vely. Võimaldab mängijal liikuda.

		if ((!võidetud)) {
			x += velx;
			y += vely;

		}

		/*
		 * tsükli abil kas mängija on läinud suuremale alale kui palli suurus.
		 * Kui, siis ütleb "Kahjuks on mäng Teie jaoks läbi!" ja
		 * 
		 * lõpetab mängu, sest tingimus võidetud muutub(ja siis ta ei tee seda
		 * esimeses if sees olevat tegu).
		 */

		for (int i = 0; i < pallid.size(); i++) {
			Integer[] pall = pallid.get(i);
			if ((this.x > pall[0] && this.x < pall[0] + 80)
					&& (this.y > pall[1] && this.y < pall[1] + 80) && !võidetud) {
				System.out.println("Kahjuks on mäng Teie jaoks läbi!");
				võidetud = true;
			}
		}

		// kontrollin kas mängija on ruudul. Kui ta on, siis
		// väljastab:"Palju õnne, Te võitsite!" ja lõpetab mängu.

		if ((350.0 < this.x && this.x < 430.0)
				&& (200.0 < this.y && this.y < 280.0) && !võidetud) {

			võidetud = true;
			System.out.println("Palju õnne! Te võitsite!");

		}

		// kontrollib kas on läinud nendele aladele, kus ringid asuvad(Pidi väga
		// halb kood selle pärast olema).

		if ((400.0 < this.x && this.x < 460.0)
				&& (100.0 < this.y && this.y < 160.0)
				|| (300.0 < this.x && this.x < 360.0)
				&& (400.0 < this.y && this.y < 460.0)
				|| (100.0 < this.x && this.x < 180.0)
				&& (100.0 < this.y && this.y < 180.0) && !võidetud) {

			sleep(100);
		}

	}

	// meetodid

	public void up() {
		vely = -1.5;
		velx = 0;
	}

	public void down() {
		vely = 1.5;
		velx = 0;
	}

	public void left() {
		velx = -1.5;
		vely = 0;
	}

	public void right() {
		velx = 1.5;
		vely = 0;

	}

	public static void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (Exception e) {

		}
	}

	/*
	 * klaviatuuri kasutamine. Kui vajutatakse üles nuppu, siis tegevus, mis on
	 * up meetodi sees, kutsutakse välja ja
	 * 
	 * värvitakse uuesti üle.
	 */

	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		if (code == KeyEvent.VK_UP) {
			up();
			repaint();

		}
		if (code == KeyEvent.VK_DOWN) {
			down();
			repaint();

		}
		if (code == KeyEvent.VK_RIGHT) {
			right();
			repaint();

		}
		if (code == KeyEvent.VK_LEFT) {
			left();
			repaint();
		}
	}

	public void keyTyped(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

}
