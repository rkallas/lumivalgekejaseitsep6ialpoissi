package lumivalgekejaseistsep6ialpoissi;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Naerun2gu extends Application{

	public void start(Stage primaryStage){
		
		Pane pane = new Pane();
		
		Image image = new Image ("https://pixabay.com/en/smile-happy-happiness-happy-face-476038/");
		
		pane.getChildren().add(new ImageView(image));
		
		ImageView imageView2 = new ImageView(image);
		imageView2.setFitHeight(100);
		imageView2.setFitWidth(100);
		pane.getChildren().add(imageView2);
		
		Scene scene = new Scene(pane, 200, 200);
		primaryStage.setTitle("Naerunägu");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
