package lumivalgekejaseistsep6ialpoissi;

import java.util.Scanner;

/*
 * @author rahel
 */

//siin klassis suheldakse kasutajaga

public class Rakendus {

	public static void main(String[] args) throws InterruptedException {

		Scanner sc = new Scanner(System.in);

		System.out.println("Mis on Teie nimi?");

		String nimi = sc.nextLine();

		System.out.println("Tere, " + nimi + "!");

		Thread.sleep(1000);

		System.out
				.println("Selle rakendusega saate mängida või arvutada oma kehamassiindeksit.");

		Thread.sleep(1000);

		System.out
				.println("Kui Te soovite mängida, sisestage kümnest väiksem arv!");

		int soov = sc.nextInt();

		// Kui kasutaja soovib mängida, siis mäng, mis on Window klassis,
		// käivitub

		if (soov < 10) {

			Window.main(null);

		} else {

			// Kui kasutaja ei soovi mängida(sisestab suurem kui 10 arvu),
			// siis...

			System.out.println("Kahju, et Te ei soovi mängida.");

			Thread.sleep(1000);

			System.out
					.println("Kehamassiindeksi arvutamiseks kuvatakse kohe üks aken.");

			Thread.sleep(1000);

			javafx.application.Application.launch(Kehamassiindeks.class);
		}
	}
}
