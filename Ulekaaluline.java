package lumivalgekejaseistsep6ialpoissi;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;


public class Ulekaaluline  extends JFrame{
	
	public static void uleinimene() {
		
		JFrame f = new JFrame("Ülekaaluline");
		JPanel p = new JPanel();

		f.setLocation(400, 100);
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		f.setVisible(true);
		f.setSize(300, 300);
		f.setResizable(false);
	}
	
	@Override
	public void paint(Graphics g) {
		
		super.paint(g);
		g.setColor(Color.pink);
		g.drawOval(120, 100, 100, 20);
		
		g.setColor(Color.red);
		g.drawLine(10, 7, 10, 7);
		
		g.setColor(Color.blue);
		g.drawOval(100, 100, 40, 40);
		
		g.setColor(Color.blue);
		g.drawOval(100, 100, 40, 40);
	}
}