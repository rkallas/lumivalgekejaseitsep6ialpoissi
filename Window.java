package lumivalgekejaseistsep6ialpoissi;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.WindowConstants;

public class Window extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	public static void main(String args[]) {

		// teen akna

		JFrame myFrame = new JFrame("M2ng");
		JPanel panel = new JPanel();
		Container content = myFrame.getContentPane();
		content.setLayout(new GridLayout(10, 10));

		// lisan nupu

		JButton button = new JButton();
		button.setText("ALUSTA");
		button.setActionCommand("ALUSTA");
		button.setEnabled(true);
		button.addActionListener(new Window());
		Icon warnIcon = new ImageIcon("Warn.gif");

		// mängu reeglid

		JLabel label1 = new JLabel(warnIcon);
		label1.setText("MÄNGUREEGLID:");
		label1.setHorizontalTextPosition(JLabel.LEFT);
		label1.setVerticalTextPosition(JLabel.BOTTOM);
		content.add(label1);

		JLabel label2 = new JLabel(warnIcon);
		label2.setText("1. Mängjiat kujutab mängus sinine punkt.");
		label2.setHorizontalTextPosition(JLabel.LEFT);
		label2.setVerticalTextPosition(JLabel.TOP);
		content.add(label2);

		JLabel label3 = new JLabel(warnIcon);
		label3.setText("2. Mängija ei tohi hallide pallidega kokku puutuda.");
		label3.setHorizontalTextPosition(JLabel.LEFT);
		label3.setVerticalTextPosition(JLabel.CENTER);
		content.add(label3);

		JLabel label4 = new JLabel(warnIcon);
		label4.setText("3. Rohelised pallid aeglustavad mängija liikumist.");
		label4.setHorizontalTextPosition(JLabel.LEFT);
		label4.setVerticalTextPosition(JLabel.CENTER);
		content.add(label4);

		JLabel label5 = new JLabel(warnIcon);
		label5.setText("4. Mängija eesmärgiks on jõuda mustale alale.");
		label5.setHorizontalTextPosition(JLabel.LEFT);
		label5.setVerticalTextPosition(JLabel.CENTER);
		content.add(label5);

		panel.add(button);

		// akna väljanägemine

		myFrame.add(panel);
		myFrame.setLocation(100, 100);
		myFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		myFrame.setVisible(true);
		myFrame.setSize(600, 400);
	}

	// kui vajutatakse nuppu, siis kutsutakse meetod main Level1 klassist

	public void actionPerformed(ActionEvent e) {
		Level1.main(null);
	}

}
