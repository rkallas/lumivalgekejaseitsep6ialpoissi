package lumivalgekejaseistsep6ialpoissi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

public class second extends JPanel implements ActionListener, KeyListener {
	Timer t = new Timer(5, this);
	double x = 0, y = 0, velx = 0, vely = 0;
	public Color b = new Color(200, 250, 230);

	public boolean võidetud = false;

	public second() {
		t.start();
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(b);
		Graphics2D g2 = (Graphics2D) g;
		g2.fillRect(350, 200, 80, 80);

		g2.setColor(Color.blue);
		g2.fill(new Ellipse2D.Double(x, y, 40, 40));

		g2.setColor(Color.gray);
		g2.fill(new Ellipse2D.Double(500, 300, 200, 200));

		g2.setColor(Color.gray);
		g2.fill(new Ellipse2D.Double(300, 300, 150, 150));

		g2.setColor(Color.red);
		g2.fill(new Ellipse2D.Double(20, 300, 200, 200));

		g2.setColor(Color.red);
		g2.fill(new Ellipse2D.Double(300, 30, 150, 150));

		g2.setColor(Color.gray);
		g2.fill(new Ellipse2D.Double(10, 30, 200, 200));

	}

	public void actionPerformed(ActionEvent e) {
		repaint();
		if (!võidetud) {
			x += velx;
			y += vely;
		}

		if ((500.0 < this.x && this.x < 700.0)
				&& (300.0 < this.y && this.y < 500.0)
				|| (300.0 < this.x && this.x < 450.0)
				&& (300.0 < this.y && this.y < 450.0)
				|| (10.0 < this.x && this.x < 40)
				&& (10.0 < this.y && this.y < 210)) {

			Kaotus.main(null);
			võidetud = true;
		}
		if ((350.0 < this.x && this.x < 430.0)
				&& (200.0 < this.y && this.y < 280.0)) {

			V6it.main(null);
			võidetud = true;

		}
		if ((20.0 < this.x && this.x < 220.0)
				&& (300.0 < this.y && this.y < 500.0)
				|| ((300.0 < this.x && this.x < 500) && (30.0 < this.y && this.y < 180))) {

			sleep(500);
		}
	}

	public void up() {
		vely = -1.5;
		velx = 0;
	}

	public void down() {
		vely = 1.5;
		velx = 0;
	}

	public void left() {
		velx = -1.5;
		vely = 0;
	}

	public void right() {
		velx = 1.5;
		vely = 0;
	}

	public static void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (Exception e) {
		}

	}

	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		if (code == KeyEvent.VK_UP) {
			up();
		}
		if (code == KeyEvent.VK_DOWN) {
			down();
		}
		if (code == KeyEvent.VK_RIGHT) {
			right();
		}
		if (code == KeyEvent.VK_LEFT) {
			left();
		}
		if (code == KeyEvent.VK_ENTER) {
			sleep(2000);
		}
	}

	public void keyTyped(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}
}
